package com.flycms.common.exception;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 自定义异常
 *
 * @author 孙开飞
 */
public class PageAssert extends Assert {

  public static void isNull(Object object, String message) {
    if (object != null) {
      throw new ApiException(message);
    }
  }

  public static void isNull(Object object, int code, String message) {
    if (object != null) {
      throw new ApiException(code, message);
    }
  }

  public static void notNull(Object object, String message) {
    if (object == null) {
      throw new ApiException(message);
    }
  }

  public static void notNull(Object object, int code, String message) {
    if (object == null) {
      throw new ApiException(code, message);
    }
  }

  public static void isTrue(boolean expression, String message) {
    if (!expression) {
      throw new PageException(message);
    }
  }

  public static void isTrue(boolean expression, int code, String message) {
    if (!expression) {
      throw new PageException(code, message);
    }
  }

  public static void notTrue(boolean expression, String message) {
    if (expression) {
      throw new PageException(message);
    }
  }

  public static void notTrue(boolean expression, int code, String message) {
    if (expression) {
      throw new PageException(code, message);
    }
  }

  public static void isEmpty(String txt, String message) {
    if (!StringUtils.isEmpty(txt)) {
      throw new PageException(message);
    }
  }

  public static void isEmpty(String txt, int code, String message) {
    if (!StringUtils.isEmpty(txt)) {
      throw new PageException(code, message);
    }
  }

  public static void notEmpty(String txt, String message) {
    if (StringUtils.isEmpty(txt)) {
      throw new PageException(message);
    }
  }

  public static void notEmpty(String txt, int code, String message) {
    if (StringUtils.isEmpty(txt)) {
      throw new PageException(code, message);
    }
  }
}
