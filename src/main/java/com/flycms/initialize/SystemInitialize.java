package com.flycms.initialize;

import com.flycms.common.utils.spring.SpringContextUtil;
import com.flycms.modules.system.service.ConfigureService;
import com.flycms.modules.shiro.ShiroTag;
import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 程序部分参数初始化服务启动
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 13:05 2019/8/17
 */
@Service
public class SystemInitialize {

    @Autowired
    private Configuration configuration;
    @Autowired
    private ShiroTag shiroTag;
    @PostConstruct
    public void setSharedVariable() throws TemplateModelException {
        ConfigureService demoAnnotationService = SpringContextUtil.getBean(ConfigureService.class);
            //网站名称
            configuration.setSharedVariable("global_site_name", demoAnnotationService.findByKeyCode("global_site_name"));
            //网站SEO搜索的关键字，首页根内页没有设置keywords的都默认用此
            configuration.setSharedVariable("global_site_keywords", demoAnnotationService.findByKeyCode("global_site_keywords"));
            //网站SEO描述，首页根内页没有设置description的都默认用此
            configuration.setSharedVariable("global_site_description", demoAnnotationService.findByKeyCode("global_site_description"));
            //模板路径
            configuration.setSharedVariable("global_templets_skin", demoAnnotationService.findByKeyCode("global_templets_skin"));
            // shiro鉴权
            configuration.setSharedVariable("shiro", shiroTag);
    }
}
