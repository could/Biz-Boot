package com.flycms.modules.help.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.help.entity.Help;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface HelpDao extends BaseDao<Help> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按网站id查询该网站下是否已添加同名帮助信息
     *
     * @param siteId
     *         网站id
     * @param title
     *         帮助信息标题
     * @param id
     *         需要排除id
     * @return
     */
    public int checkHelpByTitle(@Param("siteId") Long siteId, @Param("title") String title, @Param("id") Long id);
}
