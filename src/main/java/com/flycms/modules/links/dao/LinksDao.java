package com.flycms.modules.links.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.links.entity.Links;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface LinksDao extends BaseDao<Links> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按网站id查询该网站下是否已有同名网站
     *
     * @param siteId
     *         网站id
     * @param linkName
     *         友情链接网站名称
     * @param id
     *         需要排除id
     * @return
     */
    public int checkLinksByLinkName(@Param("siteId") Long siteId, @Param("linkName") String linkName, @Param("id") Long id);
}
