package com.flycms.modules.links.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 网站友情链接实体类
 * @email 79678111@qq.com
 * @Date: 22:48 2019/12/5
 */
@Setter
@Getter
public class Links implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private int linkType;
    private Long siteId;
    private String linkName;
    private String linkUrl;
    private String linkLogo;
    private Boolean isShow;
    private int sortOrder;
    private LocalDateTime createTime;
    private int deleted;
}
