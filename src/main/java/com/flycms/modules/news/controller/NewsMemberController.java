 package com.flycms.modules.news.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.exception.PageAssert;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.news.entity.News;
import com.flycms.modules.news.entity.NewsDO;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.entity.SiteColumn;

import net.sf.json.JSONArray;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

 /**
  * Biz-Boot, All rights reserved
  * 版权：企业之家网 -- 企业建站管理系统<br/>
  * 开发公司：97560.com<br/>
  *
 * 图文、新闻
 * @author 孙开飞
 */
@Controller
@RequestMapping("/member/news")
public class NewsMemberController extends BaseController {
	 // ///////////////////////////////
	 // /////       增加       ////////
	 // ///////////////////////////////
	 @RequestMapping("/add{url.suffix}")
	 public String add(@RequestParam(value = "siteId", required = false) String siteId,
							  @RequestParam(value = "columnId", required = false) String columnId,
							  Model model){
		 ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
		 ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteId)), "您不是网站管理员，请勿非法操作");
		 ApiAssert.notTrue(columnId == null || !NumberUtils.isNumber(columnId), "分类id为空或者类型错误");
		 SiteColumn column=siteColumnService.findByIdAndSiteId(Long.parseLong(columnId),Long.parseLong(siteId));
		 ApiAssert.notTrue(column == null, "该分类不存在");
		 List<String> fatherNode = siteColumnService.resultNodeList(Long.parseLong(siteId),Long.parseLong(columnId));
         Collections.reverse(fatherNode);
		 JSONArray json  =  JSONArray.fromObject(fatherNode);
		 String  fatherNodeJson  =  json.toString();
		 model.addAttribute("fatherNode",fatherNodeJson);
		 model.addAttribute("siteId",siteId);
		 model.addAttribute("column",column);
		 return theme.getAdminTemplate("system/member/site/news/add");
	 }

     @PostMapping("/add{url.suffix}")
     @ResponseBody
     public Object add(NewsDO newsDO)
     {
         ApiAssert.notTrue(newsDO.getSiteId() == null || !NumberUtils.isNumber(newsDO.getSiteId()), "网站id为空或者类型错误");
         ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(newsDO.getSiteId())), "您不是网站管理员，请勿非法操作");
         ApiAssert.notTrue(newsDO.getColumnId() == null || !NumberUtils.isNumber(newsDO.getColumnId()), "分类id为空或者类型错误");
         SiteColumn column=siteColumnService.findByIdAndSiteId(Long.parseLong(newsDO.getColumnId()),Long.parseLong(newsDO.getSiteId()));
         ApiAssert.notTrue(column == null, "该分类不存在");
         if(StringUtils.isEmpty(newsDO.getTitle())){
             return Result.failure("标题不能为空");
         }
         if(StringUtils.isEmpty(newsDO.getContent())){
             return Result.failure("内容不能为空");
         }
		 Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
         News news=new News();
		 news.setTitle(newsDO.getTitle());
		 news.setShorttitle(newsDO.getShorttitle());
		 news.setTitlepic(newsDO.getTitlepic());
		 news.setWriter(newsDO.getWriter());
		 news.setSource(newsDO.getSource());
		 news.setColumnId(Long.parseLong(newsDO.getColumnId()));
		 news.setSiteId(Long.parseLong(newsDO.getSiteId()));
		 news.setKeywords(newsDO.getKeywords());
		 news.setDescription(newsDO.getDescription());
		 news.setContent(newsDO.getContent());
		 news.setStatus(newsDO.getStatus());
         return newsService.addUserNews(news,company.getId(),1);
     }
	 // ///////////////////////////////
	 // /////        刪除      ////////
	 // ///////////////////////////////
	 //批量删除网站
	 @PostMapping("/del{url.suffix}")
	 @ResponseBody
	 public Object deleteSiteByIds(String ids){
		 if(StringUtils.isEmpty(ids)){
			 return Result.failure("文章id为空或者不存在");
		 }
		 return newsService.deleteNewsByIds(ids);
	 }
	 // ///////////////////////////////
	 // /////        修改      ////////
	 // ///////////////////////////////
	 //编辑网站基本信息
	 @GetMapping("/edit{url.suffix}")
	 public String edit(@RequestParam(value = "id", required = false) String id, Model model)
	 {
		 if (!NumberUtils.isNumber(id)) {
			 return "redirect:/404.do";
		 }
		 PageAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(id)), "您不是网站管理员，请勿非法操作");
		 News news=newsService.findById(Long.parseLong(id));
		 model.addAttribute("news",news);
		 return "system/member/site/news/edit";
	 }

	 //保存文章信息
	 @PostMapping("/edit{url.suffix}")
	 @ResponseBody
	 public Object edit(News news)
	 {
		 return newsService.updateUserNewsById(news);
	 }


	 // ///////////////////////////////
	 // /////        查詢      ////////
	 // ///////////////////////////////

}