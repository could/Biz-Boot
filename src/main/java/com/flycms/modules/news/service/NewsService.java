package com.flycms.modules.news.service;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.news.entity.News;
import com.flycms.modules.news.entity.NewsVO;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 新闻、图文相关 服务层接口
 *
 * @author 孙开飞
 */
public interface NewsService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public Object addUserNews(News news, Long companyId, Integer typeId);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 批量删除内容
     *
     * @param ids
     * @return
     */
    public Result deleteNewsByIds(String ids);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    public Object updateUserNewsById(News news);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按网站id查询该网站下是否有同标题文章
     *
     * @param siteId
     *         网站id
     * @param title
     *         文章标题
     * @param id
     *         需要排除id
     * @return
     */
    public boolean checkNewsByTitle(Long siteId,String title,Long id);

    /**
     * 按id查询文章内容
     *
     * @param id
     * @return
     */
    public News findById(Long id);

    /**
     * 基于layui的资讯翻页列表
     *
     * @param news
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectNewsListLayPager(News news, Integer page, Integer pageSize, String sort, String order);

    /**
     * 管理后台基于layui的资讯翻页查询列表
     *
     * @param news
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectAdminNewsListLayPager(News news, Integer page, Integer pageSize, String sort, String order);

    public Pager<NewsVO> selectNewsListPager(News news, Integer page, Integer pageSize, String sort, String order);
}
