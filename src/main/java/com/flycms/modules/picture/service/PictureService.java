package com.flycms.modules.picture.service;

import com.flycms.modules.picture.entity.Picture;
import com.flycms.modules.picture.entity.PictureInfo;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 16:36 2019/11/4
 */
public interface PictureService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 添加图片信息
     *
     * @param picture
     * @return
     * @throws Exception
     */
    public boolean addUserPicture(Picture picture) throws Exception;

    /**
     * 添加信息和图片库关联信息
     *
     * @param info
     * @return
     */
    public Object savePictureInfo(PictureInfo info);

    /**
     * 更新内容时对已有的图片数据分析本地化路径处理
     *
     * @param content
     *         需要分析的内容
     * @param companyId
     *         企业id
     * @param typeId
     *         图片类型
     * @param infoId
     *         信息id
     * @return
     * @throws Exception
     */
    public String replaceContent(String content, Long companyId, Integer typeId, Long infoId);

    /*
     *
     * 内容下载图片保存路径设置
     */
    public String getImgPath();
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除图片主表信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 更新统计当前图片使用数量
     *
     * @param countInfo
     * @param id
     * @return
     */
    public int updatePictureInfoCount(Integer countInfo,Long id);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按企业id和图片唯一指纹查询是否存在
     *
     * @param companyId
     * @param signature
     * @return
     */
    public boolean checkPictureBySignature(Long companyId,String signature);

    /**
     * 按信息id和图片id查询是否存在
     *
     * @param pictureId
     *         图片id
     * @param infoId
     *         信息id
     * @return
     */
    public boolean checkPictureByInfoId(Long pictureId,Long infoId);

    /**
     * 按图片指纹和企业id查询图片被使用的数量
     *
     * @param companyId
     * @param signature
     * @return
     */
    public int queryPictureByInfoTotal(Long companyId,String signature);

    /**
     * 按id查询图片内容
     *
     * @param id
     * @return
     */
    public Picture findById(Long id);

    /**
     * 按企业id和图片唯一指纹查询图片信息
     *
     * @param companyId
     * @param signature
     * @return
     */
    public Picture findPictureBySignature(Long companyId,String signature);

    /**
     * java计算文件32位md5值,生成图片指纹用户排除重复内容
     * @param fis 输入流
     * @return
     */
    public String md5HashCode32(InputStream fis);
}
