package com.flycms.modules.site.service;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 0:05 2019/10/18
 */
public interface ContentService {

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按分类id查询该分类内容翻页列表
     *
     * @param columnId
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectContentListPager(String siteId,String columnId, String title, Integer page, Integer pageSize, String sort, String order);
}
