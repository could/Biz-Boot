package com.flycms.modules.site.service;

import com.flycms.modules.site.entity.SiteColumn;
import com.flycms.modules.site.entity.SiteColumnListVO;
import com.flycms.modules.site.entity.SiteColumnTree;
import com.flycms.modules.site.entity.SiteColumnVO;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 站点栏目
 * @author 孙开飞
 */
public interface SiteColumnService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 用户添加企业网站分类
     *
     * @param siteColumn
     * @return
     */
    public Object addUserSiteColumn(SiteColumn siteColumn);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 用户编辑企业网站分类
     *
     * @param siteColumn
     * @return
     */
    public Object editUserSiteColumn(SiteColumn siteColumn);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按id查询分类信息
     *
     * @param id
     * @return
     */
    public SiteColumn findById(Long id);

    /**
     * 按分类id加网站id查询分类信息
     *
     * @param id
     *         分类id
     * @param siteId
     *         网站id
     * @return
     */
    public SiteColumn findByIdAndSiteId(Long id,Long siteId);

    /**
     * 按分类id加网站id查询父级分类名
     *
     * @param id
     *         分类id
     * @param siteId
     *         网站id
     * @return
     */
    public String parentName(Long id,Long siteId);

    /**
     * 按网站id查询分类树
     *
     * @param siteId
     * @return
     */
    public List<SiteColumnTree> findColumnTreeBySiteId(Long siteId);

    /**
     * 按网站id查询和分类树，传递模型id判断设置状态
     *
     * @param siteId
     * @param channel
     * @return
     */
    public List<SiteColumnTree> findColumnTreeBySiteId(Long siteId,Long channel);

    /**
     * 按网站ID和分类父级id查询分类列表
     *
     * @return
     */
    public List<SiteColumn> selectColumnListByParentId(Long siteId, Long parentId);

    /**
     * 按网站ID查询所有分类列表提供给前台网站树
     *
     * @return
     */
    public List<SiteColumnListVO> selectColumnListTreetable(Long siteId);

    /**
     * 按网站ID和分类父级id查询分类列表
     *
     * @return
     */
    public List<SiteColumnVO> selectColumnList(Long siteId, Integer row, Long parentId);

    /**
     *
     * @param siteId
     *         网站id
     * @param id
     *         当前分类id
     * @return
     */
    public List<String> resultNodeList(Long siteId, Long id);

    /**
     * 按父级分类id查询是否属于当前分类id，并且当前id不能转移到自己id下任何分类内
     *
     * @param siteId
     *         当前选择的分类
     * @param id
     *         当前分类id
     * @return
     */
    public Boolean checkColumnNode(Long siteId, Long id, Long parentId);
}
