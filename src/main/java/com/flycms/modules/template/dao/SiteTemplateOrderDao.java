package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.SiteTemplateOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteTemplateOrderDao extends BaseDao<SiteTemplateOrder> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 检查模板购买订单是否存在
     *
     * @param siteId
     * @param templateId
     * @return
     */
    public int checkTemplateOrder(@Param("siteId") Long siteId,
                                  @Param("templateId") Long templateId);
}
