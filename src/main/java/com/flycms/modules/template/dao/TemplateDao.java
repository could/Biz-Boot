package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.Template;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface TemplateDao extends BaseDao<Template> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 排除当前模板id后查询模板名是否存在,如果id设置为null则查全部的模板名
     *
     * @param templateName
     *         模板名称
     * @param id
     *         需要排除的id,可设置为null
     * @return
     */
    public int checkTemplateByName(@Param("templateName") String templateName, @Param("id") Long id);
}
