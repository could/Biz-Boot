package com.flycms.modules.template.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.modules.template.dao.TemplateColumnDao;
import com.flycms.modules.template.entity.TemplateColumn;
import com.flycms.modules.template.service.TemplateColumnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 模板分类
 * @email 79678111@qq.com
 * @Date: 11:47 2019/9/16
 */

@Service
public class TemplateColumnServiceImpl implements TemplateColumnService {
    private static final Logger log = LoggerFactory.getLogger(TemplatePageServiceImpl.class);
    @Autowired
    private TemplateColumnDao templateColumnDao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    public int deleteById(Long id){
        return templateColumnDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询所有分类列表
     *
     * @return
     */
    public List<TemplateColumn> selectTemplateColumnList() {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        whereStr.append(" and status = 0");
        //whereStr.append(" and user_id = #{entity.userId}");
        Pager<TemplateColumn> pager = new Pager();
        //排序设置
        pager.addOrderProperty("sort_order", true,true);
        //使用limit进行查询翻页
        pager.addLimitProperty(false);
        //查询条件
        //TemplateColumn entity = new TemplateColumn();
       // entity.setUserId(ShiroUtils.getLoginUser().getId());//当前用户id
        //pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        return templateColumnDao.queryList(pager);
    }
}
