package com.flycms.modules.user.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.user.entity.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface AdminDao extends BaseDao<Admin> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 更新用户登录密码
     *
     * @param password
     * @param id
     * @return
     */
    public int updatePassword(@Param("password") String password, @Param("id") Long id);

    /**
     * 更新用户头像
     *
     * @param avatar
     * @param id
     * @return
     */
    public int updateAvatar(@Param("avatar") String avatar, @Param("id") Long id);

    /**
     * 修改用户邮箱
     *
     * @param email
     * @param id
     * @return
     */
    public int updateEmail(@Param("email") String email, @Param("id") Long id);

    /**
     * 修改用户登录手机号码
     *
     * @param phone
     * @param id
     * @return
     */
    public int updatePhone(@Param("phone") String phone, @Param("id") Long id);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 排除当前用户id后查询用户名是否存在,如果userId设置为null则查全部的用户名
     *
     * @param userName
     *         用户登录名
     * @param id
     *         需要排除的id,可设置为null
     * @return
     */
    public int checkByUserName(@Param("userName") String userName,@Param("id") Long id);

    /**
     * 通过username查询用户信息
     *
     * @param username
     * @return User
     */
    public Admin findByUsername(@Param("username") String username);
}
